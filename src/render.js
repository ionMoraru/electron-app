const { desktopCapturer, remote } = require('electron')
const { writeFile } = require('fs')
const { dialog, Menu } = remote

let mediaRecorder;
const recordedChunks = [];
const options = {
	mimeType: 'video/webm; codecs: vp9'
}

const videoElement = document.querySelector('video');
const startBtn = document.getElementById('startBtn');
const stopBtn = document.getElementById('stopBtn');
const videoSelectBtn = document.getElementById('videoSelectBtn');

videoSelectBtn.onclick = getVideoSources


startBtn.onclick = e => {
	mediaRecorder.start();
	startBtn.classList.add('is-danger');
	startBtn.innerText = 'Recording';
};

stopBtn.onclick = e => {
	mediaRecorder.stop();
	startBtn.classList.remove('is-danger');
	startBtn.innerText = 'Start';
};
async function getVideoSources() {
	const inputSources = await desktopCapturer.getSources({
		types: ['window', 'screen']
	})

	const videoOptionsMenu = Menu.buildFromTemplate(
		inputSources.map((source) => {
			return {
				label: source.name,
				click: () => selectSource(source)
			}
		})
	)

	videoOptionsMenu.popup()
}

async function selectSource(source) {
	videoSelectBtn.innerText = source.name

	const constraints = {
		audio: false,
		video: {
			mandatory: {
				chromeMediaSource: 'desktop',
				chromeMediaSourceId: source.id
			}
		}
	}

	const stream = await navigator.mediaDevices.getUserMedia(constraints)

	videoElement.srcObject = stream
	await videoElement.play()


	mediaRecorder = new MediaRecorder(stream, options)


	mediaRecorder.ondataavailable = handleDataAvailable
	mediaRecorder.onstop = handleStop


}

function handleDataAvailable(e) {
	console.log('video available')
	recordedChunks.push(e.data)
}

async function handleStop() {
	const blob = new Blob(recordedChunks, options)

	const buffer = new Buffer.from(await blob.arrayBuffer())
	const { filePath } = await dialog.showSaveDialog({
		buttonLabel: 'save video',
		defaultPath: `vid-${Date.now()}.webm`
	})

	console.log(filePath)

	writeFile(filePath, buffer, () => console.log('video saved successfully !'))
}
